module.exports = function() {
    this.dbConnections = [];

    this.dbConnections["m"] = {
        host: process.env.EndPoint_rdsM,
        port: process.env.Port_rdsM,
        user: process.env.User_rdsM,
        password: process.env.Password_rdsM,
        database: "m"
    };
};